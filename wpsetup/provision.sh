#!/usr/bin/env bash
#
# WP Setup provisioning file
#

# Install phpmyadmin
DBPASSWD=root
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
apt-get -y install phpmyadmin > /dev/null 2>&1

# update phpmyadmin to v4.8.4
cd /usr/share/
sudo rm -rf phpmyadmin
sudo wget https://files.phpmyadmin.net/phpMyAdmin/4.8.4/phpMyAdmin-4.8.4-english.zip
sudo unzip phpMyAdmin-4.8.4-english.zip
sudo mv phpMyAdmin-4.8.4-english phpmyadmin


# Install WP-CLI
echo "== Update WP CLI (re-install) =="
cd ../../../
sudo rm usr/local/bin/wp
sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
echo "WP-CLI Update (re-install) successful"


# update WP-CLI
echo "== Update WP CLI =="
sudo wp cli update --yes --allow-root


# increase upload size
echo "== Update Max Filesize for PHP 5 =="
sudo sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 64M/g' /etc/php5/apache2/php.ini
sudo sed -i 's/post_max_size = 8M/post_max_size = 64M/g' /etc/php5/apache2/php.ini
echo "== Update Max Filesize for PHP 7.0 =="
sudo sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 64M/g' /etc/php/7.0/apache2/php.ini
sudo sed -i 's/post_max_size = 8M/post_max_size = 64M/g' /etc/php/7.0/apache2/php.ini
sudo service apache2 restart


# run WP Setup
echo "== Run WP Setup =="
cd ../../var/www/wpsetup
sudo -u vagrant bash setup.sh