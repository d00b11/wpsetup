# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# WP setup Vagrantfile using Scotch Box
#
# File Version: 1.0

Vagrant.configure("2") do |config|
    config.vm.box = "scotch/box"
    config.vm.network "private_network", ip: "192.168.33.200"
    config.vm.network "forwarded_port", guest: 80, host: 8080
    config.vm.hostname = "sw.wp"
    config.vm.synced_folder ".", "/var/www", :mount_options => ["dmode=777", "fmode=666"]

    # Customize the amount of memory on the VM:
    config.vm.provider "virtualbox" do |v|
        v.memory = 1024
        v.cpus = 2
end

    # Windows Support
    if Vagrant::Util::Platform.windows?
      config.vm.provision "shell",
      inline: "echo \"Converting Files for Windows\" && sudo apt-get install -y dos2unix && cd /var/www/ && dos2unix wpsetup/config.yml && dos2unix wpsetup/provision.sh && dos2unix wpsetup/setup.sh",
      run: "always", privileged: false
    end

    # Run Provisioning – executed within the first `vagrant up` and every `vagrant provision`
    config.vm.provision "shell", path: "wpsetup/provision.sh"

    config.vm.provision "shell", inline: "/home/vagrant/.rbenv/shims/mailcatcher --http-ip=0.0.0.0", run: "always"

    # OPTIONAL - Update WordPress and all Plugins on vagrant up – executed within every `vagrant up`
    #config.vm.provision "shell", inline: "echo \"== Update WordPress & Plugins ==\" && cd /var/www/public && wp core update && wp plugin update --all", run: "always", privileged: false

    # OPTIONAL - Enable NFS. Make sure to remove line 13 (See https://stefanwrobel.com/how-to-make-vagrant-performance-not-suck)
    #config.vm.synced_folder ".", "/var/www", :nfs => { :mount_options => ["dmode=777","fmode=666"] }

    config.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update
      sudo apt-get install -y libapache2-mod-php7.0 php7.0-xml
      sudo service apache2 restart
    SHELL
    
end
